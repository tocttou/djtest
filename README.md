## OpenCV online

Online pipeline builder for OpenCV. Functions can be extended very easily.

## Setup

`apt-get install python-dev`
`apt-get install libopencv-dev python-opencv`
`pip install django`
`pip install django_compressor`
`pip install django-appconf`

`apt-get install ruby`
`gem install compass`

Launch the app with `python manage.py runserver` and go to http:localhost:8000/home/ to access the page.

